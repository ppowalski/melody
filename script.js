var URL = '';
var timeCounter = 0;

$(function() {
    var connection = false;
    var ROOM_NAME = '';

    var socket = new WebSocket('ws://localhost:8080');
    socket.onopen = function(e) {
        console.log("Connection established!");
        connection = true;
        $('#loading').hide();
        $('#loginBox').show();
    };

    socket.onmessage = function(e) {
        let data = JSON.parse(e.data);

        switch(data['action']) {
            case "CREATE_ROOM":

                $('#roomName').val('');
                $('#myRooms').hide();
              
                $('#users').html('');
                $.each(data['data']['users'], function (i, element) {
                    let user = '<div class="user"><div class="text-left float-left">'+ element['login'] +'</div><div class="text-right">'+ element['points'] +'</div></div>';
                    $('#users').append(user);
                });

                $('#room-onwer').html('Właściciel: ' + data['data']['ownerName']);

                $('#room-answer').html('Odpowiadający: ' + data['data']['answer']['name']);

                $('#answer').remove();
                $('#player-console').hide();

                $('#game').show();

                break;

            case "REFRESH_ROOMS":
                    $('#roomName').val('');
                    $('#myRooms').html('');
    
                    $.each(data['data'], function (i, element) {
                        let room = '<div class="room row"><div class="col-md-8"><span style="font-size: 25px;">'+ i +'</span></div><div class="col-md-4 text-right"><button type="button" class="btn btn-primary myButton joinRoom" data-name="'+ i +'">Dołącz</button></div></div>';
                        $('#myRooms').append(room);
                    });
                    
                    break;    

            case "JOIN_ROOM":

                $('#roomName').val('');

                $('#myRooms').html('');
                $('#myRooms').hide();

                $('#users').html('');
                $.each(data['data']['users'], function (i, element) {
                    let user = '<div class="user"><div class="text-left float-left">'+ element['login'] +'</div><div class="text-right">'+ element['points'] +'</div></div>';
                    $('#users').append(user);
                });

                $('#room-onwer').html('Właściciel: ' + data['data']['ownerName']);
                $('#room-answer').html('Odpowiadający: ' + data['data']['answer']['name']);
               
                $('#music').remove();
                $('#accept').remove();
                $('#remove').remove();

                $('#game').show();

                break;   

            case "REFRESH_ROOM":
                $('#roomName').val('');

                $('#myRooms').html('');
                $('#myRooms').hide();

                $('#users').html('');
                $.each(data['data']['users'], function (i, element) {
                    let user = '<div class="user"><div class="text-left float-left">'+ element['login'] +'</div><div class="text-right">'+ element['points'] +'</div></div>';
                    $('#users').append(user);
                });

                $('#room-onwer').html('Właściciel: ' + data['data']['ownerName']);
                $('#room-answer').html('Odpowiadający: ' + data['data']['answer']['name']);

                $('#game').show();

                if(URL !== data['data']['url']) {
                    URL = data['data']['url'];

                    if(URL !== '') {
                        timeCounter = 0;
                        clearInterval(timer);

                        timer  = setInterval(function() {
                            time();
                        }, 1000);
                    }
    
                    player.loadVideoById(URL);
                }
     
                break;     

            case "REFRESH_ROOM_OWNER":
                $('#roomName').val('');

                $('#myRooms').html('');
                $('#myRooms').hide();

                $('#users').html('');
                $.each(data['data']['users'], function (i, element) {
                    let user = '<div class="user"><div class="text-left float-left">'+ element['login'] +'</div><div class="text-right">'+ element['points'] +'</div></div>';
                    $('#users').append(user);
                });

                $('#room-onwer').html('Właściciel: ' + data['data']['ownerName']);
                $('#room-answer').html('Odpowiadający: ' + data['data']['answer']['name']);

                $('#music').hide();
                $('#player-console').show();

                $('#game').show();
                
                break; 

            case "REFRESH_ROOM_OWNER_START":
                $('#roomName').val('');

                $('#myRooms').html('');
                $('#myRooms').hide();

                $('#users').html('');
                $.each(data['data']['users'], function (i, element) {
                    let user = '<div class="user"><div class="text-left float-left">'+ element['login'] +'</div><div class="text-right">'+ element['points'] +'</div></div>';
                    $('#users').append(user);
                });

                $('#room-onwer').html('Właściciel: ' + data['data']['ownerName']);
                $('#room-answer').html('Odpowiadający: ' + data['data']['answer']['name']);

                $('#player-console').hide();
                $('#music').show();

                $('#game').show();
                
                break;  
        }

        
        let stop = data['stop'] || false;
        let start = data['start'] || false;
        let pause = data['pause'] || false;

        if(stop) {
            stopVideo();
            timeCounter = 0;

            $('#counter').html(timeCounter);
        }

        
        if(pause) {
            pauseVideo();
        }

        if(start) {
            playVideo();
        }
    };

    $(document).on("click",".joinRoom",function() {
        let room = $(this).data('name');

        ROOM_NAME = room;
        
        socket.send(JSON.stringify({action: "JOIN_ROOM", name: room}));
    });

    $('#join').click(function() {
        if(connection) {
            let name = $('#login').val();

            socket.send(JSON.stringify({action: "LOGIN", name: name}));

            $('#loginBox').hide();
            $('#rooms').show();
       
        }
    });

    $('#create').click(function() {
        if(connection) {
            let roomName = $('#roomName').val() || Math.random();
            ROOM_NAME = roomName;
            socket.send(JSON.stringify({action: "CREATE_ROOM", name: roomName}));
        }
    });

    $(document).on("click","#accept",function() {
        socket.send(JSON.stringify({action: "ACCEPT", name: ROOM_NAME}));
    });

    $(document).on("click","#remove",function() {
        socket.send(JSON.stringify({action: "REMOVE", name: ROOM_NAME}));
    });

    $(document).on("click","#on",function() {
        let idmusic = $('#urlMusic').val();
        $('#urlMusic').val('');

        $('#player').addClass('admin');

        socket.send(JSON.stringify({action: "ON", name: ROOM_NAME, url: idmusic}));
    });

    $('#answer').click(function() {
        socket.send(JSON.stringify({action: "ANSWEAR", name: ROOM_NAME}));
    });
});


function time() {
    timeCounter++;
    $('#counter').html(timeCounter);
}

var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      var timer;  

      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '0',
          width: '0',
          videoId: URL,
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
      }

      var onPlayerStateChange = function(event) {

        // get video Id from event, not player.
        videoId = typeof event.target.getVideoData().video_id !== 'undefined' ? event.target.getVideoData().video_id : 'unknown';
    
        // track when user clicks Play
        if (event.data == YT.PlayerState.PLAYING) {
    
            if (window.ga && ga.loaded) {
    
                ga('send', 'event', 'Video', 'play', videoId);
            }
        }
    
        // track when user clicks Pause
        if (event.data == YT.PlayerState.PAUSED) {
    
            if (window.ga && ga.loaded) {
    
                ga('send', 'event', 'Video', 'pause', videoId);
            }
        }
    
        // track when video ends
        if (event.data == YT.PlayerState.ENDED) {
    
            if (window.ga && ga.loaded) {
    
                ga('send', 'event', 'Video', 'ended', videoId);
            }
        }
    };

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        player.getVideoData();
        event.target.playVideo();
      }

      function playVideo() {
          player.getVideoData();
          player.playVideo();

          timer  = setInterval(function() {
            time();
          }, 1000);
      }

      function stopVideo() {
        player.stopVideo();
        clearInterval(timer);
      }

      function pauseVideo() {
          player.pauseVideo();
          clearInterval(timer);
      }