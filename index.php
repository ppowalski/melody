<?php

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

require __DIR__ . '/vendor/autoload.php';

class Music implements MessageComponentInterface {
    protected $clients;

    private $users;
    private $rooms;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->rooms = [];
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
        $this->users[$conn->resourceId] = [];
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $data = json_decode($msg, true);

        switch($data['action']) {
            case 'LOGIN':
                $this->users[$from->resourceId] = [
                    'login' => $data['name'],
                ];

                $result = [
                    'action' => 'REFRESH_ROOMS',
                    'data' => $this->rooms,
                ];

                foreach ($this->clients as $client) {
                    $client->send(json_encode($result));
                }
                break;
            case 'REFRESH_ROOMS':
                    if(empty($this->rooms[$data['name']])) {
                        $this->rooms[$data['name']] = [
                            "owner" => $from->resourceId,
                            "ownerName" => $this->users[$from->resourceId]['login'],
                        ];
    
                        $result = [
                            'action' => 'REFRESH_ROOMS',
                            'data' => $this->rooms,
                        ];
    
                        foreach ($this->clients as $client) {
                            $client->send(json_encode($result));
                        }
                    }
                break;    
            case 'CREATE_ROOM':
                if(empty($this->rooms[$data['name']])) {
                    $this->rooms[$data['name']] = [
                        "owner" => $from->resourceId,
                        "ownerName" => $this->users[$from->resourceId]['login'],
                        "users" => [],
                        "url" => '',
                        'answer' => [
                            'id' => null,
                            'name' => ' - ',
                        ]
                    ];

                    $result = [
                        'action' => 'CREATE_ROOM',
                        'data' => $this->rooms[$data['name']],
                    ];

                    foreach ($this->clients as $client) {
                        $result = [
                            'action' => $from->resourceId === $client->resourceId ? 'CREATE_ROOM' : 'REFRESH_ROOMS',
                            'data' => $from->resourceId === $client->resourceId ? $this->rooms[$data['name']] : $this->rooms,
                        ];

                        $client->send(json_encode($result));
                    }
                }
                break;
                case 'JOIN_ROOM':
                    if(!empty($this->rooms[$data['name']])) {
                        $this->rooms[$data['name']]['users'][$from->resourceId] = [
                            'login' => $this->users[$from->resourceId]['login'],
                            'points' => 0,
                        ];
    
                        foreach ($this->clients as $client) {
                            if($from->resourceId === $client->resourceId) {
                                $result = [
                                    'action' => 'JOIN_ROOM',
                                    'data' => $this->rooms[$data['name']],
                                ];

                                $client->send(json_encode($result));
                            } else if($this->rooms[$data['name']]['owner'] === $client->resourceId || array_key_exists($client->resourceId, $this->rooms[$data['name']]['users'])) {
                                $result = [
                                    'action' => 'REFRESH_ROOM',
                                    'data' => $this->rooms[$data['name']],
                                ];

                                $client->send(json_encode($result));
                            }   

                        }
                    }
                    break;    

                case 'REFRESH_ROOM':
                    if(!empty($this->rooms[$data['name']])) {
                        $this->rooms[$data['name']]['users'][$from->resourceId] = [
                            'login' => $this->users[$from->resourceId]['login'],
                            'points' => 0,
                        ];

                        $result = [
                            'action' => 'REFRESH_ROOM',
                            'data' => $this->rooms[$data['name']],
                        ];
    
                        foreach ($this->clients as $client) {
                            if($this->rooms[$data['name']]['owner'] === $client->resourceId || array_key_exists($client->resourceId, $this->rooms[$data['name']]['users'])) {
                                $client->send(json_encode($result));
                            }   
                        }
                    }
                    break;  

                case 'ANSWEAR':
                    if(!empty($this->rooms[$data['name']]) && empty($this->rooms[$data['name']]['answer']['id'])) {
                        $this->rooms[$data['name']]['answer']['id'] = $from->resourceId;
                        $this->rooms[$data['name']]['answer']['name'] = $this->users[$from->resourceId]['login'];
    
                        foreach ($this->clients as $client) {
                            if(array_key_exists($client->resourceId, $this->rooms[$data['name']]['users'])) {
                                $result = [
                                    'action' => 'REFRESH_ROOM',
                                    'data' => $this->rooms[$data['name']],
                                    'pause' => true,
                                ];

                                $client->send(json_encode($result));
                            } else if($this->rooms[$data['name']]['owner'] === $client->resourceId) {
                                $result = [
                                    'action' => 'REFRESH_ROOM_OWNER',
                                    'data' => $this->rooms[$data['name']],
                                    'pause' => true,
                                ];

                                $client->send(json_encode($result));
                            }   
                        }
                    }
                    break;  

                case 'ACCEPT':
                    if(!empty($this->rooms[$data['name']]) && !empty($this->rooms[$data['name']]['answer']['id'])) {
                        
                        if(!empty($this->rooms[$data['name']]['users'][$this->rooms[$data['name']]['answer']['id']])) {
                            $this->rooms[$data['name']]['users'][$this->rooms[$data['name']]['answer']['id']]['points'] += 100;
                        }

                        $this->rooms[$data['name']]['answer']['id'] = null;
                        $this->rooms[$data['name']]['answer']['name'] = ' - ';
    
                        foreach ($this->clients as $client) {
                            if(array_key_exists($client->resourceId, $this->rooms[$data['name']]['users'])) {
                                $result = [
                                    'action' => 'REFRESH_ROOM',
                                    'data' => $this->rooms[$data['name']],
                                    'stop' => true,
                                ];

                                $client->send(json_encode($result));
                            } else if($this->rooms[$data['name']]['owner'] === $client->resourceId) {
                                $result = [
                                    'action' => 'REFRESH_ROOM_OWNER_START',
                                    'data' => $this->rooms[$data['name']],
                                    'stop' => true,
                                ];

                                $client->send(json_encode($result));
                            }   
                        }
                    }
                    break; 

                case 'REMOVE':
                    if(!empty($this->rooms[$data['name']]) && !empty($this->rooms[$data['name']]['answer']['id'])) {
                        $this->rooms[$data['name']]['answer']['id'] = null;
                        $this->rooms[$data['name']]['answer']['name'] = ' - ';
    
                        foreach ($this->clients as $client) {
                            if(array_key_exists($client->resourceId, $this->rooms[$data['name']]['users'])) {
                                $result = [
                                    'action' => 'REFRESH_ROOM',
                                    'data' => $this->rooms[$data['name']],
                                    'start' => true,
                                ];

                                $client->send(json_encode($result));
                            } else if($this->rooms[$data['name']]['owner'] === $client->resourceId) {
                                $result = [
                                    'action' => 'REFRESH_ROOM_OWNER_START',
                                    'data' => $this->rooms[$data['name']],
                                    'start' => true,
                                ];

                                $client->send(json_encode($result));
                            }   
                        }
                    }
                    break;  

                case 'ON':
                    if(!empty($this->rooms[$data['name']]) && !empty($data['url'])) {
                        $this->rooms[$data['name']]['url'] = $data['url'];
    
                        foreach ($this->clients as $client) {
                            if($this->rooms[$data['name']]['owner'] === $client->resourceId || array_key_exists($client->resourceId, $this->rooms[$data['name']]['users'])) {
                                $result = [
                                    'action' => 'REFRESH_ROOM',
                                    'data' => $this->rooms[$data['name']],
                                ];

                                $client->send(json_encode($result));
                            } 
                        }
                    }
                    break;      
        }
    }

    public function onClose(ConnectionInterface $conn) {
        $this->clients->detach($conn);
        unset($this->users[$conn->resourceId]);

        foreach($this->rooms as $key => $room) {
            if($room['owner'] === $conn->resourceId) {
                unset($this->rooms[$key]);
            }
            
            $refresh = false;
            foreach($room['users'] as $uid => $users) {
                if($uid === $conn->resourceId) {
                    unset($room['users'][$uid]);
                    unset($this->rooms[$key]['users'][$uid]);
                    $refresh = true;
                }
            }
            
            if($refresh) {
                 $result2 = [
                    'action' => 'REFRESH_ROOM',
                    'data' => $this->rooms[$key],
                 ];
            
                 foreach ($this->clients as $client) {
                     if(array_key_exists($client->resourceId, $room['users'])) {
                         $client->send(json_encode($result2));
                     }
                 }
            }
        }

        $result = [
            'action' => 'REFRESH_ROOMS',
            'data' => $this->rooms,
        ];

        foreach ($this->clients as $client) {
            $client->send(json_encode($result));
        }
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }
}


$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Music()
        )
    ),
    8080
);

$server->run();